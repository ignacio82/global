MODULE optimizer
use global_minimum
IMPLICIT NONE
contains
subroutine find_minimum(X0, F0)
   DOUBLE PRECISION                 :: MIN(15), MAX(15)
   integer                          :: NPARM, NSAMPL, NSEL, IPR, NSIG, M, NC
   DOUBLE PRECISION, intent(out)    :: X0(15,20), F0(20)
      M = 1
      NPARM = 2
      NSAMPL = 100
      NSEL = 2
      IPR = 6

      OPEN(6, FILE='OUTPUT')

      NSIG = 6
      MIN(1) = -100.0
      MAX(1) =  100.0
      MIN(2) = -100.0
      MAX(2) =  100.0

      CALL GLOBAL(MIN, MAX, NPARM, M, NSAMPL, NSEL, IPR, NSIG, X0, NC, F0)
end subroutine find_minimum

END MODULE optimizer
