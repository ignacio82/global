PROG =	global.X

SRCS =	global.f90 main.f90 objective_f_module.f90 optimizer_module.f90

OBJS =	global.o main.o objective_f_module.o optimizer_module.o

LIBS =	

FC = ifort
FFLAGS = -O
F90 = ifort
F90FLAGS = -O
LDFLAGS = 
all: $(PROG)

$(PROG): $(OBJS)
	$(F90) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

.PHONY: clean
clean:
	rm -f $(PROG) $(OBJS) *.mod

.SUFFIXES: $(SUFFIXES) .f .f90 .F90 .f95
.SUFFIXES: $(SUFFIXES) .c .cpp .cxx

.f90.o .f95.o .F90.o:
	$(F90) $(F90FLAGS) -c $<

.f.o:
	$(FC) $(FFLAGS) -c $<

global.o: objective_f_module.o
main.o: optimizer_module.o
optimizer_module.o: global.o
