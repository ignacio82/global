#SHORT DESCRIPTION OF THE GLOBAL OPTIMIZATION SUBROUTINE
Global optimization is a part of nonlinear optimization, it deals with
problems with (possibly) several local minima. The presented method is
stochastic (i.e. not deterministic). The framework procedure, the GLOBAL
routine gives a computational evidence, that the best local minimum found is
with high probability the global minimum.  This routine calls a local search
routine, and a routine for generating random numbers.

The subroutines are written in FORTRAN.  The user has to provide a main program
doing the input - output, and a subroutine capable of computing the objective
function value for any point in the parameter space.  There were different
versions for IBM-compatible mainframes and personal computers.  The subroutine
GLOBAL can be used for the approximate solution of the global minimization
problem, as follows:

Let F(X) be a real function of NPARM parameters and we are looking for
parameter values X(I) from the given intervals [MIN(I), MAX(I)] for each
I = 1, 2, ..., NPARM.  The problem is to determine such a point X*, that the
function value F(X) is greater than or equal to F(X*) for every X in the
NPARM-dimensional interval specified by MIN(I)'s and MAX(I)'s.

##To get this code and run it just do the following:

```BASH
mkdir bitbucket
cd bitbucket
git clone git@bitbucket.org:ignacio82/global.git
cd global
make
./global.X
```

#I got the library from [Alan Miller's website](http://jblevins.org/mirror/amiller/)