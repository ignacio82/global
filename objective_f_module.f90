MODULE objective_f
IMPLICIT NONE
CONTAINS

subroutine FUNCT(x, VALUE, nparm, m)
   integer, intent(in)                             :: nparm, m
   DOUBLE PRECISION ,dimension(nparm), intent(in)  :: x
   DOUBLE PRECISION                  , intent(out) :: VALUE
   VALUE = 100.0*(X(1)*X(1)-X(2))**2 + (1.0-X(1))**2!1.0 - COS(X(1)) + (X(1)/100.0)**2
end subroutine FUNCT

END MODULE objective_f
